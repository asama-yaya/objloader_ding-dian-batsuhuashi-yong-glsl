#pragma once

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
// OpenGL --------------------------------------------------------------------------------------
#include <glew.h>
#include <glut.h>
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "glut32.lib")
// MyHeader ------------------------------------------------------------------------------------
#include "AsaVertex.h"
#include "AsaTrackball.h"



// Defines
#define OBJ_BUFFER_LENGTH 1024
#define OBJ_NAME_LENGTH 256


/////////////////////////////////////////////////////////////////////////
// OBJVERTEX
/////////////////////////////////////////////////////////////////////////
struct OBJVERTEX
{
	Vertex3Df position;
	Vertex3Df normal;
	Vertex2Df texcoord;
	OBJVERTEX(){}
};

/////////////////////////////////////////////////////////////////////////
// OBJSUBSET
/////////////////////////////////////////////////////////////////////////
struct OBJSUBSET
{
	unsigned int materialIndex;
	unsigned int faceStart;
	unsigned int faceCount;
	OBJSUBSET(){}
};

/////////////////////////////////////////////////////////////////////////
// OBJMATERIAL
/////////////////////////////////////////////////////////////////////////
struct OBJMATERIAL
{
	char name[OBJ_NAME_LENGTH];
	Vertex3Df ambient;
	Vertex3Df diffuse;
	Vertex3Df specular;
	float shininess;
	double alpha;
	std::string ambientMapName;
	std::string diffuseMapName;
	std::string specularMapName;
	std::string bumpMapName;
	OBJMATERIAL(){}
};

/////////////////////////////////////////////////////////////////////////
// OBJBOUNDINGBOX
/////////////////////////////////////////////////////////////////////////
struct OBJBOUNDINGBOX
{
	Vertex3Df maximum;
	Vertex3Df minimum;
	Vertex3Df size;

	// 最大値を求める
	Vertex3Df OBJVEC3Max(Vertex3Df v, Vertex3Df _max)
	{
		Vertex3Df result;
		result.x = (v.x > _max.x ? v.x : _max.x);
		result.y = (v.y > _max.y ? v.y : _max.y);
		result.z = (v.z > _max.z ? v.z : _max.z);
		return result;
	}

	// 最小値を求める
	Vertex3Df OBJVEC3Min(Vertex3Df v, Vertex3Df _min)
	{
		Vertex3Df result;
		result.x = (v.x < _min.x ? v.x : _min.x);
		result.y = (v.y < _min.y ? v.y : _min.y);
		result.z = (v.z < _min.z ? v.z : _min.z);
		return result;
	}

	void Merge(Vertex3Df value){
		maximum = OBJVEC3Max(value, maximum);
		minimum = OBJVEC3Min(value, minimum);
		size = maximum - minimum;
	}
	OBJBOUNDINGBOX() {}
	OBJBOUNDINGBOX(Vertex3Df value): minimum(value), maximum(value){}
};

/////////////////////////////////////////////////////////////////////////
// OBJBOUNDINGSPHERE
/////////////////////////////////////////////////////////////////////////
struct OBJBOUNDINGSPHERE
{
	Vertex3Df center;
	double radius;
	void Create(OBJBOUNDINGBOX box){
		center = box.maximum + box.minimum;
		center = center / 2.0;

		Vertex3Df half;
		half = box.maximum - center;
		radius = half.abs();
	}
	OBJBOUNDINGSPHERE() {}
};

/////////////////////////////////////////////////////////////////////////
// OBJMESH
/////////////////////////////////////////////////////////////////////////
class OBJMESH
{
private:
	std::vector<OBJVERTEX> m_Vertices;
	std::vector<OBJSUBSET> m_Subsets;
	std::vector<OBJMATERIAL> m_Materials;
	std::vector<GLuint> m_texID;
	std::vector<unsigned int> m_Indices;
	unsigned int m_NumVertices;
	unsigned int m_NumSubsets;
	unsigned int m_NumMaterials;
	unsigned int m_NumIndices;
	OBJBOUNDINGBOX m_Box;
	OBJBOUNDINGSPHERE m_Sphere;
	std::string m_directoryPath;

	// vertexBufferとindexBuffer
	void SetVertexBuffer();
	GLuint vertexBuffer;
	std::vector<GLuint> indexBuffer;

	bool LoadMTLFile(const std::string _filename);
	bool LoadOBJFile(const std::string _filename);

public:
	OBJMESH();
	~OBJMESH();

	bool LoadFile(const std::string _filename);
	void Release();
	void Draw();
	unsigned int GetNumVertices(){ return m_NumVertices; }
	unsigned int GetNumSubsets(){ return m_NumSubsets; }
	unsigned int GetNumMaterials(){ return m_NumMaterials; }
	unsigned int GetNumIndices(){ return m_NumIndices; }
	unsigned int  GetIndexData(unsigned int index){ return m_Indices[index]; }
	unsigned int* GetIndices(){ return m_Indices.data(); }
	OBJVERTEX  GetVertex(unsigned int index){ return m_Vertices[index]; }
	OBJVERTEX* GetVertices(){ return m_Vertices.data(); }
	OBJSUBSET  GetSubset(unsigned int index){ return m_Subsets[index]; }
	OBJSUBSET* GetSubsets(){ return m_Subsets.data(); }
	OBJMATERIAL  GetMaterial(unsigned int index){ return m_Materials[index]; }
	OBJMATERIAL* GetMaterials(){ return m_Materials.data(); }
	OBJBOUNDINGBOX GetBox(){ return m_Box; }
	OBJBOUNDINGSPHERE GetSphere(){ return m_Sphere; }
};
