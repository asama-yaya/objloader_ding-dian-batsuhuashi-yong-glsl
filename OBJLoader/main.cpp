#include "main.h"


int main(int argc, char *argv[]){

	// glutのコールバック関数
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(window_width, window_height);
	glutCreateWindow("MainWindow");
	glutDisplayFunc(display);
	glutReshapeFunc(resize);
	glutMouseFunc(mouse);
	glutMotionFunc(motion);
	glutKeyboardFunc(keyboard);
	glutIdleFunc(idle);
	
	// 初期化
	glClearColor(0.0f, 0.3f, 0.0f, 0.0f);
	glEnable(GL_DEPTH_TEST);
	
	glDisable(GL_CULL_FACE);					// 両面
	//glEnable(GL_CULL_FACE);					// 表のみ
	//glCullFace(GL_BACK);
	//glEnable(GL_CULL_FACE);					// 裏のみ
	//glCullFace(GL_FRONT);

	glEnable(GL_LIGHTING); 
	glEnable(GL_LIGHT0);
	GLfloat lightpos[3] = { 0, 0, 200 };
	glLightfv(GL_LIGHT0, GL_POSITION, lightpos);
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);

	// glew初期化
	glewInit();

	// obj load
	if (!Objmesh.LoadFile("data\\untitled.obj")){
		getchar();
		exit(0);
	}

	// main loop
	glutMainLoop();
	return 0;
}

// display
void display(void){

	// 画面クリア
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// viewport
	glViewport(0, 0, window_width, window_height);

	// 透視投影うんぬん
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60, (double)window_width / window_height, 0.1, 1200.0);
	gluLookAt(0, 0, 200, 0, 0, 0, 0, 1, 0);

	// モデルビュー変換行列の初期化
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glMultMatrixd(Trackball.GetMatrix());
	Objmesh.Draw();
	//glutSolidCube(30);

	// ダブルバッファリング
	glutSwapBuffers();
}


//********************************************************
// glutのコールバック関数
//********************************************************
void resize(int _w, int _h)
{
	// トラックボールする範囲
	Trackball.Resize(_w, _h);

	//// ウィンドウ全体をビューポートにする
	//glViewport(0, 0, _w, _h);

	//// 透視変換行列の指定
	//glMatrixMode(GL_PROJECTION);

	//// 透視変換行列の初期化 
	//glLoadIdentity();
	//gluPerspective(60.0, (double)_w / (double)_h, 1.0, 100.0);
}

void idle(void)
{
	// 画面の描き替え
	glutPostRedisplay();
}

void mouse(int _button, int _state, int _x, int _y)
{
	if (_button == GLUT_LEFT_BUTTON){
		if (_state == GLUT_DOWN){
			Trackball.Click(_x, _y);
		}
		else if (_state == GLUT_UP){
			Trackball.Up();
		}
		glutIdleFunc(idle);
		return;
	}
}

void motion(int _x, int _y)
{
	 // トラックボール移動
	Trackball.Drag(_x, _y);
	 glutPostRedisplay();
}

void keyboard(unsigned char _key, int _x, int _y)
{
	// ESC か q をタイプしたら終了
	if (_key == 'q' || _key == '\033'){
		cout << "おしり";
		exit(0);
	}
	else if (_key == 'R') Trackball.uni();


	glutPostRedisplay();
}








