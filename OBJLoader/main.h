#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <Windows.h>
#include <math.h>
#include <time.h>

using std::cout;
using std::endl;

using std::vector;
using std::string;
using std::to_string;
using std::stoi;
using std::stod;
using std::stoul;

using std::ifstream;
using std::ofstream;
using std::ios;

// OpenCV---------------------------------------------------------------------------------------
#include <opencv2/opencv.hpp>		// インクルードファイル指定 
#include <opencv2/opencv_lib.hpp>	// 静的リンクライブラリの指定
// OpenGL --------------------------------------------------------------------------------------
#include <glew.h>
#include <glut.h>
#pragma comment(lib, "glew.lib")
#pragma comment(lib, "glut32.lib")
// MyHeader ------------------------------------------------------------------------------------
#pragma once
#include "AsaVertex.h"
#include "AsaTrackball.h"
#include "OBJLoader.h"

//**********************************************************************************************
// variable
//**********************************************************************************************
int window_width = 640;
int window_height = 480;

TTrackball Trackball;
OBJMESH Objmesh;


//**********************************************************************************************
// Method
//**********************************************************************************************
// glの関数
void display(void);
void resize(int _w, int _h);
void mouse(int _button, int _state, int _x, int _y);
void motion(int _x, int _y);
void keyboard(unsigned char _key, int _x, int _y);
void idle(void);